// map<T, V>(callback: (element: T) => V, collection: T[]): V[]

/**
 * @template T,V
 * @type {function} - function implementation Array.prototype.map
 * @param {function(element: T) : V} callback - A function that modifies an element of an array
 * @param {T[]} collection - Source array
 * @returns {V[]} - Output array
 */

const map = (callback, collection) => {
  return collection.reduce((mappedArray, element) => [...mappedArray, callback(element)], [])
}

console.log(map(item => item + 5, [1, 2, 3, 4, 5]))
