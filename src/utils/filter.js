// filter<T>(predicate: (element: T) => boolean, collection: T[]): T[]

/**
 * @template T
 * @type {function} - function implementation Array.prototype.filter
 * @param {function(element: T): boolean} predicate - A function that returns an array element that satisfies the condition
 * @param {T[]} collection - Source array
 * @returns {T[]} - Output array
 */

const filter = (predicate, collection) => {
  return collection.reduce((filteredArray, element) =>
    (predicate(element) ? [...filteredArray, element] : filteredArray), [])
}

console.log(filter(item => item > 2, [1, 2, 3, 4, 5]))
